sfini.state package
===================

Submodules
----------

.. toctree::

   sfini.state.choice

Module contents
---------------

.. automodule:: sfini.state
    :members:
    :undoc-members:
    :show-inheritance:
